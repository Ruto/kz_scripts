#!/bin/bash

MAPDIR='./maps'
KZMAPS='https://kzmaps.tangoworldwide.net/bsps'
MAPCYCLE=$( curl -s 'https://kzmaps.tangoworldwide.net/mapcycles/gokz.txt' )
MAPCOUNT=$( echo "$MAPCYCLE" | wc -l )
API_MAPS=$( curl -s 'http://kztimerglobal.com/api/v1/maps?is_validated=true' )

echo "$MAPCOUNT maps"

mkdir -p "$MAPDIR"
i=0
while IFS= read -r map; do
    (( i++ ))
    REMOTE_SIZE=$( curl -sI "$KZMAPS/$map.bsp" | grep -i 'Content-Length' | awk '{print $2}' | sed 's/[^a-zA-Z0-9]//g' ) # awk returning some weird stuff so sed cleans it
    status='No change'
    update=false
    if [ -e "$MAPDIR/$map.bsp" ]; then
        # Validate against remote size
        API_SIZE=$( echo "$API_MAPS" | jq ".[]  | select(.name == \"$map\") | .filesize" )
        if [ "$API_SIZE" = "$REMOTE_SIZE" ]; then
            status='API MATCH'
        else
            update=true
        fi
    else
        update=true
    fi
    if [ $update = true ]; then
        # Download
        echo "Downloading $map"
        curl -#o "$MAPDIR/$map.bsp" "$KZMAPS/$map.bsp"
        echo -ne "\033[1A\033[2K\033[1A\033[2K" # https://unix.stackexchange.com/a/26592 http://www.tldp.org/HOWTO/Bash-Prompt-HOWTO/x361.html
        status='Downloaded'
    fi

    printf "%-3s %-30s %20s %20s\n" "$i." "$map" "[$status]" "$REMOTE_SIZE"
done <<< "$MAPCYCLE"